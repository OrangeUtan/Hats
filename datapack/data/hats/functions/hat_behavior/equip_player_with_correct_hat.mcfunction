####################################################################
# as: Player                                                       #
# Descr: Replace the #hat on the Players head with an #hat_on_head #
####################################################################

# Miscellaneous
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3000}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/fez
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3001}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/squid
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3002}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/arrow
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3003}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/frying_pan

# Tophats
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3100}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/white
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3101}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/orange
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3102}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/magenta
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3103}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/light_blue
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3104}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/yellow
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3105}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/lime
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3106}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/pink
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3107}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/gray
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3108}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/light_gray
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3109}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/cyan
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3110}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/purple
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3111}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/blue
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3112}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/brown
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3113}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/green
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3114}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/red
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3115}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/black
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3116}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/rainbow
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3117}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/tophats/black_monocle

# Cats
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3120}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/ocelot
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3121}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/tabby
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3122}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/tuxedo
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3123}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/red
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3124}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/siamese
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3125}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/british_shorthair
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3126}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/calico
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3127}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/persian
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3128}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/ragdoll
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3129}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/white
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3130}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/jellie
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3131}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/cats/black

# Glasses
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3140}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/glasses/sunglasses
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3141}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/glasses/harry_potter
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3142}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/glasses/half_rim
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3143}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/glasses/rainbow
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3144}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/glasses/librarian
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3145}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/glasses/three_d

# Accessories
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3150}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/accessories/ninja_headband
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3151}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/accessories/steve_mask
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3152}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/accessories/alex_mask
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3153}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/accessories/snorkel_mask_blue
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3154}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/accessories/snorkel_mask_red
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3155}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/accessories/googly_eyes

# Villager related
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3160}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/villager/armorer
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3161}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/villager/farmer
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3162}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/villager/nose

# Mario
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3170}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/mario_cap
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3171}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/luigi_cap
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3172}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/cappy
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3173}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/toad_red
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3174}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/toad_blue
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3175}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/toad_yellow
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3176}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/toad_green
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3177}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/mario
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:3178}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/mario/luigi

# Halloween
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:7773180}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/halloween/wiggly_ghast
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:7773181}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/halloween/native_american_headband
execute as @s[nbt={Inventory:[{Slot:103b,tag:{CustomModelData:7773182}}]}] run loot replace entity @s armor.head loot hats:hat_on_head/halloween/jason_mask